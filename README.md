# ArcCell-rs

ArcCell-rs is an experimental crate that aims to provide `ArcCell<T>`, a safe wrapper around `AtomicPtr<T>`.

It should be equivalent to Java's `AtomicReference`?

## A note on safety

This is currently **EXPERIMENTAL**. You shouldn't use this except to test it and let me know if it works as intended.

This also means its semantics may change.

# License

This crate is distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See [LICENSE-APACHE](./LICENSE-APACHE) and [LICENSE-MIT](./LICENSE-MIT) for details.
